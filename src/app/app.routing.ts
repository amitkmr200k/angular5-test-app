//import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './products/products.component';
// import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

// Route Configuration
export const routes: Routes = [
  {path: 'products', component: ProductsComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {path: 'home', component: HomeComponent},
  // {path: '**', component: ProductsComponent},

];

//export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
