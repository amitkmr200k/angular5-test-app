import { Component, OnInit } from '@angular/core';

import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})

export class TopNavComponent implements OnInit {
  productsList: Array<any>;	

  constructor(private commonService: CommonService) { 
  	this.productsList = this.commonService.getProductsList();
  }

  ngOnInit() {
  }

}
