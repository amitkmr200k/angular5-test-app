import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CommonService {

  constructor(private http: Http) { }

  getProductsList() 
  {
  	//data:Array<any> = ['1', '2'];
  	return ['mobile', 'tablet'];
  }

  getProducts() 
  {
  	return this.http.get('assets/data/products.json')
  		.map(res => res.json());
  }


}
