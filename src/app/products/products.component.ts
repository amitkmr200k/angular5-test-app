import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';

@Component({
    selector: 'products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {

    products;
    error: boolean = false;

    constructor(private commonService: CommonService) {}

    ngOnInit() {
        this.commonService.getProducts()
        .subscribe(
            data => {
                this.products = data
            },
            err => {
                console.log(err);
                this.error = true;
            }
        );
    }

}
